﻿namespace CombineVideo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.lblVideo1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lblVideo2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(496, 38);
            this.button1.TabIndex = 0;
            this.button1.Text = "Video 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblVideo1
            // 
            this.lblVideo1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblVideo1.Location = new System.Drawing.Point(0, 38);
            this.lblVideo1.Name = "lblVideo1";
            this.lblVideo1.Size = new System.Drawing.Size(496, 23);
            this.lblVideo1.TabIndex = 3;
            this.lblVideo1.Text = "label1";
            this.lblVideo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Location = new System.Drawing.Point(0, 61);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(496, 38);
            this.button2.TabIndex = 6;
            this.button2.Text = "Video 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Location = new System.Drawing.Point(0, 122);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(496, 38);
            this.button3.TabIndex = 9;
            this.button3.Text = "Combine Demm";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblVideo2
            // 
            this.lblVideo2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblVideo2.Location = new System.Drawing.Point(0, 99);
            this.lblVideo2.Name = "lblVideo2";
            this.lblVideo2.Size = new System.Drawing.Size(496, 23);
            this.lblVideo2.TabIndex = 8;
            this.lblVideo2.Text = "label2";
            this.lblVideo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 155);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.lblVideo2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblVideo1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblVideo1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lblVideo2;
    }
}

