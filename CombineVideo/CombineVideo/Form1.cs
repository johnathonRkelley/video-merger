﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineVideo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblVideo1.Text = getFilePath(); 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string path = Application.StartupPath + "\\" + "ffmpeg.win32.exe";
            string[] data = output();

            string textfile = Application.StartupPath + "\\" + "text.txt";
            System.IO.File.WriteAllLines(textfile, data);

            string outputLocation = Application.StartupPath + "\\" + "merged.mp4";
            Process p = new Process();
            p.StartInfo.UseShellExecute = true; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = false;
            p.StartInfo.FileName = path;
            p.StartInfo.Arguments = " -f concat -i " + textfile + " -c copy -fflags +genpts " + outputLocation;
            //p.StartInfo.Arguments = " -f concat -i text.txt -c copy -fflags +genpts merged.mp4";
            //p.StartInfo.Arguments = " -y -ss " + currentVideo.getStart() + " -i " + currentVideo.getFilename() + " -t " + (currentVideo.getStop() - currentVideo.getStart()) + " -c copy " + currentVideo.getClip();
            Console.WriteLine(path + p.StartInfo.Arguments);
            p.Start();
            p.WaitForExit();
        }

        public String getFilePath()
        {
            String filePath = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Video Files",

                CheckFileExists = true,
                CheckPathExists = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,

                Filter = "Video Files (*.mp4)|*.mp4"
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filePath = openFileDialog1.FileName;
            }

            return filePath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lblVideo2.Text = getFilePath();
        }

        public String[] output()
        {
            String[] arr = new String[2];
            arr[0] = "file " +  '\u0027' + lblVideo1.Text + '\u0027';
            arr[1] = "file " +  '\u0027' + lblVideo2.Text + '\u0027';

            return arr;
        }
    }
}
