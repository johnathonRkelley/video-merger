﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombineVideo
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnAddFile_Click(object sender, EventArgs e)
        {

        }
        
        public String getFilePath()
        {
            String filePath = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Video Files",

                CheckFileExists = true,
                CheckPathExists = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,

                Filter = "Video Files (*.mp4)|*.mp4"
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filePath = openFileDialog1.FileName;
            }

            return filePath;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                if (listView1.Items[i].Selected == true)
                {
                    listView1.Items[i].Remove();
                }
            }
        }

        private void btnAddFile_Click_1(object sender, EventArgs e)
        {
            String filepath = getFilePath();

            listView1.Items.Add(new ListViewItem(filepath));
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            string path = Application.StartupPath + "\\" + "ffmpeg.win32.exe";
            string[] data = output();

            string textfile = Application.StartupPath + "\\" + "text.txt";
            System.IO.File.WriteAllLines(textfile, data);

            string outputLocation = Application.StartupPath + "\\" + "merged.mp4";
            Process p = new Process();
            p.StartInfo.UseShellExecute = false; 
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = path;
            p.StartInfo.Arguments = " -y -f concat -i " + textfile + " -c copy -fflags +genpts " + outputLocation;
            p.Start();
            p.WaitForExit();

            listView1.Items.Clear();
        }

        public String[] output()
        {
            String[] arr = new String[listView1.Items.Count];

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                arr[i] = "file " + '\u0027' + listView1.Items[i].Text + '\u0027';          
            }

            return arr;
        }
    }
}
